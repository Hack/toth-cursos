<?php

class Cadastro_model extends CI_Model{

	private $DbCadastro = null;


	function __construct()
	{
		$this->DbCadastro = $this->load->database('default', TRUE);
	}


	public function Salvar($dados)
	{
		if(!empty($dados)){

			$this->DbCadastro->insert(
				'usuarios',
				 array(
					"id_status" => 1, 
					"nome" => $dados['nome'],
					"email" => $dados['email'], 
					"senha" => md5($dados['senha']),
					"data_cadastro" => date("Y-m-d H:i:s")
				)
			);		

			$id_usuario = $this->DbCadastro->insert_id();

			if(!empty($id_usuario)){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

}