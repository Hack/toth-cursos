<?php

class Curso_model extends CI_Model{

	private $DbCurso = null;

	function __construct()
	{
		$this->DbCurso = $this->load->database('default', TRUE);
	}


	public function Get($busca = array())
	{
		$where = "";

		if(!empty($busca)){
			foreach($busca as $coluna => $valor){
				$valor['tipo'] = strtolower((isset($valor['tipo']) && !empty($valor['tipo']) ? $valor['tipo'] : "string"));

				switch($valor['tipo']){
					default:
					case "string":
						$where .= "AND {$coluna} = '{$valor['valor']}' ";
					break;

					case "int":
						$where .= "AND {$coluna} = {$valor['valor']} ";
					break;

					case "like":
						$where .= "AND {$coluna} LIKE '%{$valor['valor']}%'";
					break;
				}
			}
		}

		$sql = "
			SELECT C.*,CC.*, P.professor FROM cursos AS C
			INNER JOIN cursos_categoria AS CC ON CC.id_categoria = C.id_categoria
			INNER JOIN professores AS P ON P.id_professor = C.id_professor
			WHERE 1 {$where} 
			ORDER BY C.data_cadastro DESC";

		$cursos = $this->DbCurso->query($sql)->result_array();

		if(!empty($cursos)){
			return $cursos;
		}else{
			return false;
		}
	}

	public function QuantidadeCursosCategoria()
	{
		$sql = "
			SELECT CC.categoria, COUNT(1) AS total 
			FROM cursos AS C 
			INNER JOIN cursos_categoria AS CC ON CC.id_categoria = C.id_categoria 
			GROUP BY C.id_categoria 
			ORDER BY CC.categoria ASC";

		$quantidade = $this->DbCurso->query($sql)->result_array();		

		if(!empty($quantidade)){
			return $quantidade;
		}else{
			return false;
		}
	}

	public function Professores()
	{
		$sql = "SELECT professor FROM professores ORDER BY professor ASC";
		$professores = $this->DbCurso->query($sql)->result_array();		

		if(!empty($professores)){
			return $professores;
		}else{
			return false;
		}
	}

}