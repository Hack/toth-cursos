<?php

class Professor_model extends CI_Model{

	private $DbProfessor = null;

	function __construct()
	{
		$this->DbProfessor = $this->load->database('default', TRUE);
	}


	public function Get($busca = array())
	{
		$where = "";

		if(!empty($busca)){
			foreach($busca as $coluna => $valor){
				$valor['tipo'] = strtolower((isset($valor['tipo']) && !empty($valor['tipo']) ? $valor['tipo'] : "string"));

				switch($valor['tipo']){
					default:
					case "string":
						$where .= "AND {$coluna} = '{$valor['valor']}' ";
					break;

					case "int":
						$where .= "AND {$coluna} = {$valor['valor']} ";
					break;

					case "like":
						$where .= "AND {$coluna} LIKE '%{$valor['valor']}%'";
					break;
				}
			}
		}

		$sql = "SELECT * FROM professores WHERE 1 {$where}";
		$professores = $this->DbProfessor->query($sql)->result_array();

		if(!empty($professores)){
			return $professores;
		}else{
			return false;
		}
	}

}