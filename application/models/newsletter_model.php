<?php

class Newsletter_model extends CI_Model{

	private $DbNewsletter = null;


	function __construct()
	{
		$this->DbNewsletter = $this->load->database('default', TRUE);
	}

	private function Mensagem($chave = "erro")
	{
		$tag_inicial = "<span class='status-{$chave}'>";
		$mensagem['ja_cadastrado'] = "Email já inscrito para receber nossa newsletter";
		$mensagem['erro'] = "Ocorre um erro ao se inscrever para receber nossa newsletter. Por favor, tente mais tarde.";
		$mensagem['sucesso'] = "Email inscrito com sucesso.";
		$tag_final = "</span>";

		return $tag_inicial.$mensagem[$chave].$tag_final;
	}

	private function VerificaInscricao($email)
	{
		if(!empty($email)){
			$sql = $this->DbNewsletter->query("SELECT id_newsletter FROM newsletter WHERE email = '{$email}'");

			if($sql->num_rows() > 0){
				return true;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}


	public function Inscrever($email)
	{
		if(!empty($email)){
			if($this->VerificaInscricao($email)){
				return $this->Mensagem("ja_cadastrado");
			}

			$this->DbNewsletter->insert(
				'newsletter',
				 array(
					"email" => $email, 
					"data_cadastro" => date("Y-m-d H:i:s")
				)
			);		

			$id_newsletter = $this->DbNewsletter->insert_id();

			if(!empty($id_newsletter)){
				return $this->Mensagem("sucesso");
			}else{
				return $this->Mensagem("erro");
			}
		}else{
			return $this->Mensagem("erro");
		}
	}

}