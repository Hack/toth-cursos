<?php

class Contato_model extends CI_Model{

	private $DbContato = null;

	function __construct()
	{
		$this->DbContato = $this->load->database('default', TRUE);
	}

	private function Mensagem($chave = "erro")
	{
		$tag_inicial = "<span class='status-{$chave}'>";
		$mensagem['erro'] = "Ocorre um erro ao enviar seu contato. Por favor, tente mais tarde.";
		$mensagem['sucesso'] = "Contato enviado com sucesso. Em breve, entraremos em contato.";
		$tag_final = "</span>";

		return $tag_inicial.$mensagem[$chave].$tag_final;
	}

	public function Enviar($dados)
	{
		if(!empty($dados)){
			$this->load->library('email');

			$this->email->from('contato@tothcursos.com.br');
			$this->email->to('contato@tothcursos.com.br');
			$this->email->subject('[Contato] Toth Cursos Online');
			$this->email->message('Testando email');
			$this->email->send();

			if($this->email->send()){
				$this->DbContato->insert(
					'contato',
					 array(
						"nome" => $dados['nome'], 
						"sobrenome" => $dados['sobrenome'],
						"email" => $dados['email'], 
						"telefone" => $dados['telefone'],
						"mensagem" => $dados['mensagem'],
						"data_cadastro" => date("Y-m-d H:i:s")
					)
				);		

				$contato = $this->DbContato->insert_id();

				if(!empty($contato)){			
					return $this->Mensagem("sucesso");
				}else{
					return $this->Mensagem("erro");
				}
			}else{
				return $this->Mensagem("erro");
			}
		}else{
			return $this->Mensagem("erro");
		}
	}

}