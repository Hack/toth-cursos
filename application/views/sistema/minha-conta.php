<section id="sub-header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<h1>Nome do aluno</h1>
			</div>
		</div>
	</div>
	<div class="divider_top"></div>
</section>


<section id="main_content">

<div class="container">

<ol class="breadcrumb">
  <li><a href="<?php echo base_url();?>">Home</a></li>
  <li class="active">Minha Conta</li>
</ol>
      <div class="row">
         <div class="col-md-12">
     
     				<!--  Tabs -->   
                    <ul class="nav nav-tabs" id="mytabs">
                        <li class="active"><a href="#profile" data-toggle="tab">Perfil</a></li>
                        <li><a href="#courses" data-toggle="tab">Meus Cursos</a></li>
                        <li><a href="#agenda" data-toggle="tab">Minha Agenda</a></li>
                    </ul>
                    
                    <div class="tab-content">
                    
                        <div class="tab-pane fade in active" id="profile">
                        	<div class="row">
                                	 <aside class="col-md-4">
                                        <div class=" box_style_1 profile">
                                        <p class="text-center"><img src="<?php echo base_url('public/img/aluno.jpg');?>" alt="Aluno" class="img-circle styled" width="200" height="200"></p>
                                            <ul>
                                                <li>Nome <strong class="pull-right">Nome aluno</strong> </li>
                                                <li>Email <strong class="pull-right">emailaluno@email.com</strong></li>
                                                <li>Telefone  <strong class="pull-right">(11) xxxx-xxxx</strong></li>
                                                <li>Cidade<strong class="pull-right">Guarulhos</strong></li>
                                                <li>Sexo <strong class="pull-right">Masculino</strong></li>
                                                <li>Nível Escolar <strong class="pull-right">Superior Completo</strong></li>
                                                <li>Idade <strong class="pull-right">22</strong></li>
                                            </ul>
                                            </div><!-- End box-sidebar -->
                                     </aside><!-- End aside -->
                        	<div class="col-md-8">
                                <h3>Biografia</h3>
                                <p>Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Nullam mollis.. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapiPhasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel</p>
                                <p>Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Nullam mollis.. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapiPhasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel</p>
                                <p>Sed pretium, ligula sollicitudin laoreet viverra, tortor libero sodales leo, eget blandit nunc tortor eu nibh. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapibus id, mattis vel, nisi. Nullam mollis.. Phasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel, dapiPhasellus hendrerit. Pellentesque aliquet nibh nec urna. In nisi neque, aliquet vel</p>
                           </div><!-- End col-md-8 -->
                        </div><!-- End row -->
                       </div><!-- End tab-pane --> 
                       
                        <div class="tab-pane fade" id="courses">
                           <h3>Meus Cursos</h3>
                                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Phasellus hendrerit. </p>
                                <div class="table-responsive">
                                  <table class="table">
                                    <thead>
                                      <tr>
                                        <th>Categoria</th>
                                        <th>Curso</th>
                                        <th>Data Início</th>
                                        <th>Progresso</th>
                                        <th>Downloads</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                      <tr>
                                        <td>Empresarial</td>
                                        <td><a href="#">Marketing</a></td>
                                        <td>20/04/2015</td>
                                        <td><img src="img/bullet_complete_2.png" alt=""> 100%</td>
                                        <td><a href="#"><i class="icon-video"></i> Vídeos</a></td>
                                      </tr>
                                      <tr>
                                        <td>Engenharia</td>
                                        <td><a href="#">Engenharia de Produção</a></td>
                                        <td>20/04/2015</td>
                                        <td><img src="img/bullet_progress_2.png" alt=""> 50%</td>
                                        <td><a href="#"><i class="icon-mic"></i> Áudio</a></td>
                                      </tr>
                                      <tr>
                                        <td>Computação</td>
                                        <td><a href="#">Sistemas de Informação</a></td>
                                        <td>20/04/2015</td>
                                        <td><img src="img/bullet_start_2.png" alt=""> 100%</td>
                                        <td><a href="#"><i class="icon-doc"></i> Apostila</a></td>
                                      </tr>
                                      <tr>
                                        <td>Admnistração</td>
                                        <td><a href="#">Qualidade</a></td>
                                        <td>20/04/2015</td>
                                        <td><img src="img/bullet_start_2.png" alt=""> 100%</td>
                                        <td><a href="#"><i class="icon-doc"></i> Apostila</a></td>
                                      </tr>
                                    </tbody>
                                  </table>
                                  </div>
                        </div><!-- End tab-pane --> 
                        
                         <div class="tab-pane fade" id="agenda">
                         
                             <div class="row">
                                  <aside class="col-md-4">
                                  	 <div class="box_style_1"  id="external-events">
                                     	<h4>Draggable Events</h4>
                                            <div class='external-event'>Coffe Break</div>
                                            <div class='external-event'>Meeting</div>
                                            <div class='external-event'>Lesson</div>
                                            <div class='external-event'>Exam</div>
                                            <p><input type='checkbox' id='drop-remove' /><label for='drop-remove'>remove after drop</label></p>
                                     </div>
                                  </aside>
                                  
                                  <div class="col-md-8">
                                  <div id="calendar"></div><!-- End calendar --> 
                                  </div>
                                  
                             </div><!-- End row --> 
                        
                       	</div><!-- End tab-pane --> 
                        
                        <div class="tab-pane fade" id="plans">
                             <h3>Change your Payment method</h3>
                            <div  id="payment_opt">
                                <label class="radio-inline">
                                <input type="radio" id="" value="" name="payment" checked><img src="img/logo_paypal.png" alt="Paypal" class="payment_logos">
                                </label>
                                 <label class="radio-inline">
                                <input type="radio" id="" value="" name="payment"><img src="img/logo_visa.png" alt="Card" class="payment_logos">
                                </label>
                                 <label class="radio-inline">
                                <input type="radio" id="" value="" name="payment"><img src="img/logo_master.png" alt="Card" class="payment_logos">
                                </label>
                                 <label class="radio-inline">
                                <input type="radio" id="" value="" name="payment"><img src="img/logo_maestro.png" alt="Card" class="payment_logos">
                                </label>
                            </div>       
                            <hr>                     
                       
                            <h3>Order summary</h3>
              			 <div class="table-responsive">             
                                <table class="table table-hover " style="margin-bottom:0;">
                                    <thead>
                                    <tr>								
                                        <th>Items</th>
                                        <th>Amount</th>									
                                    </tr></thead>
                                <tbody>
                                <tr>
                                    <td>Price of the course</td>
                                    <td>€0.99</td>
                                </tr>
                                 <tr class="info" style="border-top: 2px solid #ccc; border-bottom: 2px solid #ccc; font-size:18px">
                                    <td><strong>TOTAL</strong></td>
                                    <td><strong>€0.99</strong></td>
                                </tr>
                                <tr>
                                    <td><small>without VAT</small></td>
                                    <td><small>€ 0.83</small></td>
                                </tr>
                                <tr>
                                    <td><small>VAT (19%)</small></td>
                                    <td> <small>€0.16</small> </td>
                                </tr>
                              
                            </tbody></table>
                        </div>
                    </div><!-- End tab-pane --> 
     		
         </div><!-- End col-md-8-->
      </div>   
    </div><!-- End row-->   
</div><!-- End container -->
</section><!-- End main_content-->