<section id="sub-header" >
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1>Nome do curso</h1>
				<p class="lead">Frase motivacional para o curso</p>
			</div>
		</div>       
	</div>
	<div class="divider_top"></div>
</section>

<section id="main_content">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li><a href="<?php echo base_url('cursos');?>">Cursos</a></li>
			<li class="active">Nome do Curso</li>
		</ol>

		<div class="row">
			<div class="col-md-8">
				<h2>Detalhes</h2>
				<p>
					Lorem ipsum dolor sit amet, ius minim gubergren ad. At mei sumo sonet audiam, ad mutat elitr platonem vix. Ne nisl idque fierent vix. Ferri clita ponderum ne duo, simul appellantur reprehendunt mea an. An gloriatur vulputate eos, an sed fuisset vituperatoribus, tation tritani prodesset ex sed. Impedit torquatos sed ad, vel ad placerat necessitatibus, in quo inani eligendi.
				</p>
				<p>
					Cum copiosae pertinacia ei, admodum volumus cum ut, erat nonumy his te. Iudico praesent sed id, nam error consequat reprehendunt no. Nostrud nostrum tacimates mei ut, debet facilisi in ius, dolor accusam omittam eu sea. His harum verterem ocurreret ei.
				</p>

				<hr class="add_bottom_30">

				<div class="media">
					<div class="circ-wrapper course_detail pull-left">
						<h3>01</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Capítulo 01 - Autem possim his</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-clock"></i> Duração: 6 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Duração: 6 horas</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-video"></i> Vídeo Aula
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Vídeo Aula</strong>
									</div>
									<div class="po-body">
										<ul class="list_po_body">
											<li><i class="icon-ok"></i> 2 Vídeo aula</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula prático</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula questionário</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-mic"></i> Aulas em áudio
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Aulas em áudio</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li >
								<a href="javascript:void(0)" >
									<i class=" icon-download"></i> Grade curricular
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="media">
					<div class="circ-wrapper course_detail pull-left">
						<h3>02</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Capítulo 02 - Autem possim his</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-clock"></i> Duração: 6 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Duração: 6 horas</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-video"></i> Vídeo Aula
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Vídeo Aula</strong>
									</div>
									<div class="po-body">
										<ul class="list_po_body">
											<li><i class="icon-ok"></i> 2 Vídeo aula</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula prático</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula questionário</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-mic"></i> Aulas em áudio
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Aulas em áudio</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li >
								<a href="javascript:void(0)" >
									<i class=" icon-download"></i> Grade curricular
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="media">
					<div class="circ-wrapper course_detail pull-left">
						<h3>03</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Capítulo 03 - Autem possim his</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-clock"></i> Duração: 6 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Duração: 6 horas</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-video"></i> Vídeo Aula
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Vídeo Aula</strong>
									</div>
									<div class="po-body">
										<ul class="list_po_body">
											<li><i class="icon-ok"></i> 2 Vídeo aula</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula prático</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula questionário</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-mic"></i> Aulas em áudio
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Aulas em áudio</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li >
								<a href="javascript:void(0)" >
									<i class=" icon-download"></i> Grade curricular
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="media">
					<div class="circ-wrapper course_detail pull-left">
						<h3>04</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">Capítulo 04 - Autem possim his</h4>
						<p>
							Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
						</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-clock"></i> Duração: 6 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Duração: 6 horas</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-video"></i> Vídeo Aula
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Vídeo Aula</strong>
									</div>
									<div class="po-body">
										<ul class="list_po_body">
											<li><i class="icon-ok"></i> 2 Vídeo aula</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula prático</li>
											<li> <i class="icon-ok"></i>1 Vídeo aula questionário</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)">
									<i class="icon-mic"></i> Aulas em áudio
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Aulas em áudio</strong>
									</div>
									<div class="po-body">
										<p class="no_margin">
											Lorem ipsum dolor sit amet, has at illum dictas definitiones, ei primis indoctum torquatos nec. Vis te velit probatus, natum atomorum tincidunt nec an.
										</p>
									</div>
								</div>
							</li>
							<li >
								<a href="javascript:void(0)" >
									<i class=" icon-download"></i> Grade curricular
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>

			<aside class="col-md-4">
				<a href="#" class=" button_fullwidth-3">Comprar Curso</a> 
				<div class="box_style_1">
					<h4>Valor <span class="pull-right">R$ 199,00</span></h4>
					<h4>Categoria <span class="pull-right">Administração</span></h4>
					<h4>Professor(a)<span class="pull-right">Flora Regina</span></h4>
					<h4>Aulas <span class="pull-right">17</span></h4>
					<h4>Horas <span class="pull-right">12</span></h4>                                   
				</div>
			</aside>
		</div>
	</div>
</section>

<section id="join">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2 text-center">
				<div class="row">
					<div class="col-md-2 hidden-sm hidden-xs">
						<img src="<?php echo base_url('public/img/arrow_hand_1.png');?>" alt="Seta" >
					</div>
					<div class="col-md-8">
						<a href="#" class="button_big">Comprar Curso</a> 
					</div>
				</div>
			</div>
		</div>
	</div>
</section>