<section id="sub-header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<h1>Cursos</h1>
				<p class="lead boxed">Ex utamur fierent tacimates duis choro an</p>
			</div>
		</div>
	</div>
	<div class="divider_top"></div>
</section>

<section id="main_content">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li class="active">Cursos</li>
		</ol>

		<div class="row">
			<aside class="col-lg-3 col-md-4 col-sm-4">
				<div class="box_style_1">
					<h4>Categorias</h4>
					<ul class="submenu-col">
						<?php					
							if(isset($categoria_ativa)){
						?>
								<li>
									<a href="<?php echo base_url('cursos/');?>">Todos os cursos</a>
								</li>
						<?php
							}else{
						?>
								<li>
									<a href="<?php echo base_url('cursos/');?>" id="active">Todos os cursos</a>
								</li>
						<?php
							}

							if(!empty($quantidade_categorias)){
								foreach ($quantidade_categorias as $key => $value) {
									$ativo = "";

									if(isset($categoria_ativa) && $categoria_ativa == $value['url']){
										$ativo = "id='active'";
									}
						?>
									<li>
										<a href='<?php echo base_url("cursos/categoria/{$value['url']}");?>' <?php echo $ativo;?>>
											<?php echo "{$value['categoria']} ({$value['total']})";?>
										</a>
									</li>
						<?php
								}
							}
						?>
					</ul>
				</div>

				<div class="box_style_1">
					<h4>Professores</h4>
					<ul class="submenu-col">
						<?php					

							if(!empty($professores)){
								foreach ($professores as $key => $value) {
									$ativo = "";

									if(isset($professor_ativo) && $professor_ativo == $value['url']){
										$ativo = "id='active'";
									}
						?>
									<li>
										<a href='<?php echo base_url("cursos/professor/{$value['url']}");?>' <?php echo $ativo;?>>
											<?php echo "{$value['professor']}";?>
										</a>
									</li>
						<?php
								}
							}
						?>
					</ul>
				</div>				
			</aside>

			<div class="col-lg-9 col-md-8 col-sm-8">
				<div class="row">
					<?php
						if(!empty($cursos)){
							foreach ($cursos as $key => $value) {
					?>
								<div class="col-lg-4 col-md-6">
									<div class="col-item">
										<div class="photo">
											<a href='<?php echo base_url("cursos/{$value['url']}");?>'>
												<img src="<?php echo base_url('public/img/cursos/poetry.jpg');?>" alt="" />
											</a>
											<div class="cat_row">
												<a href='<?php echo base_url("cursos/categoria/{$value['url_categoria']}");?>'><?php echo $value['categoria'];?></a>
												<span class="pull-right">
													<i class=" icon-clock"></i><?php echo "{$value['duracao']} horas";?>
												</span>
											</div>
										</div>
										<div class="info">
											<div class="row">
												<div class="course_info col-md-12 col-sm-12">
													<h4><?php echo $value['curso'];?></h4>
													<?php echo $value['resumo'];?>
													<div class="rating">
														<i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class="icon-star"></i><i class=" icon-star-empty"></i>
													</div>
													<div class="price pull-right">R$ <?php echo $value['preco'];?></div>
												</div>
											</div>
											<div class="separator clearfix">
												<p class="btn-details"> 
													<a href='<?php echo base_url("cursos/{$value['url']}");?>'><i class=" icon-list"></i> Detalhes</a>
												</p>
												<p class="btn-add">
													<a href='<?php echo base_url("cursos/{$value['url']}");?>'><i class="icon-export-4"></i> Comprar</a>
												</p>
											</div>
										</div>
									</div>
								</div>
					<?php								
							}
						}else{
					?>
							<div class="centralizado">
								<p>Nenhum curso encontrado</p>
							</div>
					<?php
						}
					?>
				</div>
			</div>
		</div>
	</div>
</section>