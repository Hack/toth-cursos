		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<h3>Inscreva-se para receber novas notícias</h3>
						<div id="message-newsletter"></div>
						<form method="post" action="<?php echo base_url('newsletter');?>" name="form-newsletter" id="newsletter" class="form-inline">
							<input name="email" id="email_newsletter" type="email" value="" placeholder="Email" class="form-control" required="">
							<button id="submit-newsletter" class=" button_outline">Inscrever</button>
						</form>
					</div>
				</div>
			</div>

			<hr>

			<div class="container" id="nav-footer">
				<div class="row text-left">
					<div class="col-md-2 col-sm-2 margin-top-footer">
						<ul>
							<li>
								<a href="<?php echo base_url('institucional');?>"><h4>Institucional</h4></a>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-sm-2 margin-top-footer">
						<ul>
							<li>
								<a href="<?php echo base_url('cursos');?>"><h4>Cursos</h4></a>
							</li>
						</ul>
					</div>
					<div class="col-md-2 col-sm-2 margin-top-footer">
						<ul>
							<li>
								<a href="<?php echo base_url('cadastro');?>"><h4>Cadastro</h4></a>
							</li>
						</ul>					
					</div>
					<div class="col-md-2 col-sm-2 margin-top-footer">
						<ul>
							<li>
								<a href="<?php echo base_url('contato');?>"><h4>Contato</h4></a>
							</li>
						</ul>
					</div>					
					<div class="col-md-4 col-sm-4">
						<img src="<?php echo base_url('public/img/pagseguro-2.png');?>" id="img-pagseguro">
					</div>
				</div>
			</div>
			<div id="copy_right">© <?php echo date("Y");?> Toth Cursos Online- Todos direitos reservados</div>
		</footer>

		<div id="toTop">Ir para topo</div>

		<script src="<?php echo base_url('public/js/jquery-1.10.2.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/rs-plugin/js/jquery.themepunch.plugins.min.js');?>"></script>
		<script type="text/javascript" src="<?php echo base_url('public/rs-plugin/js/jquery.themepunch.revolution.min.js');?>"></script>
		<script type="text/javascript">
			var revapi;

			$(document).ready(function() {
				revapi = $('.tp-banner').revolution({
					delay:9000,
					startwidth:1200,
					startheight:500,
					hideThumbs:true,
					navigationType:"none"
				});
			});
		</script>
		<script src="<?php echo base_url('public/js/superfish.js');?>"></script>
		<script src="<?php echo base_url('public/js/bootstrap.min.js');?>"></script>
		<script src="<?php echo base_url('public/js/retina.min.js');?>"></script>
		<script src="<?php echo base_url('public/js/pw_strenght.js');?>"></script>
		<script src="<?php echo base_url('public/assets/validate.js');?>"></script>
		<script src="<?php echo base_url('public/js/jquery.placeholder.js');?>"></script>
		<script src="<?php echo base_url('public/js/functions.js');?>"></script>
		<script src="<?php echo base_url('public/js/classie.js');?>"></script>
		<script src="<?php echo base_url('public/js/uisearch.js');?>"></script>
		<script>new UISearch( document.getElementById( 'sb-search' ) );</script>
	</body>
</html>