<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title><?php echo (isset($title) && !empty($title) ? $title : "Toth Cursos Online");?></title>
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1, maximum-scale=1, user-scalable=no">

        <link rel="shortcut icon" href="<?php echo base_url('public/img/favicon.ico');?>" type="image/x-icon"/>
        <link rel="apple-touch-icon" type="image/x-icon" href="<?php echo base_url('public/img/apple-touch-icon-57x57-precomposed.png');?>">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="<?php echo base_url('public/img/apple-touch-icon-72x72-precomposed.png');?>">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="<?php echo base_url('public/img/apple-touch-icon-114x114-precomposed.png');?>">
        <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="<?php echo base_url('public/img/apple-touch-icon-144x144-precomposed.png');?>">
        <link href="<?php echo base_url('public/rs-plugin/css/settings.css');?>" media="screen" rel="stylesheet">
        <link href="<?php echo base_url('public/css/bootstrap.min.cs');?>s" rel="stylesheet">
        <link href="<?php echo base_url('public/css/superfish.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('public/css/style.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('public/fontello/css/fontello.css');?>" rel="stylesheet">
        <link href="<?php echo base_url('public/css/color_scheme.css');?>" rel="stylesheet">
    </head>
    <body id="boxed"> 
        <header>
            <div class="container">
                <div class="row">
                    <div class="col-md-3 col-sm-3 col-xs-3">
                        <a href="<?php echo base_url();?>" id="logo">Toth Cursos Online</a>
                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-9">
                        <div class=" pull-right">
                            <a href="<?php echo base_url('minha-conta');?>" class="button_top" id="login_top">Entrar</a> 
                        </div>
                        <ul id="top_nav" class="hidden-xs">
                            <li>Olá, VISITANTE</li>
                        </ul>
                    </div>
                </div>
            </div>
        </header><!-- End header -->

        <nav>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div id="mobnav-btn"></div>
                        <ul class="sf-menu">
                            <li>
                                <a href="<?php echo base_url();?>">Home</a>
                            </li>
                            <li>
                                <a href="<?php echo base_url('institucional');?>">Institucional</a>
                            </li>        
                            <li class="normal_drop_down">
                                <a href="<?php echo base_url('cursos');?>">Cursos</a>
                            </li>

                            <li class="mega_drop_down">
                                <a href="<?php echo base_url('cadastro');?>">Cadastro</a>                            
                            </li>

                            <li>
                                <a href="<?php echo base_url('contato');?>">Contato</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>