<div class="boxedcontainer">
	<section class="tp-banner-container">
		<div class="tp-banner" >
			<ul class="sliderwrapper">
				<li data-transition="random-static" data-slotamount="3" data-masterspeed="1500" >
					<!-- MAIN IMAGE -->
					<img src="<?php echo base_url('public/img/banner/banner-1.jpg');?>" alt="slidebg1"  data-bgfit="cover" data-bgposition="left top" data-bgrepeat="no-repeat">
					
					<!-- LAYER NR. 2 -->
					<div class="tp-caption medium_bg_darkblue skewfromleft customout"
					data-x="left"
					data-hoffset="30"
					data-y="350"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="800"
					data-start="1500"
					data-endspeed="300"
					data-captionhidden="on"
					style="z-index: 6">Estude sem sair de casa
					</div>

					<div class="caption lfr medium_grey"
					data-x="left"
					data-hoffset="30"
					data-y="400"
					data-customout="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0.75;scaleY:0.75;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;"
					data-speed="800"
					data-start="1500"
					data-endspeed="300"
					data-captionhidden="on"
					style="z-index: 6">Cursos 100% online
					</div>
				</li>				

				

			</ul>
			<div class="tp-bannertimer"></div>
		</div>
	</section>
</div>