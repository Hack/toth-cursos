<?php echo $this->load->view("layout/banner");?>

<section id="main-features">
	<div class="divider_top_black"></div>
	<div class="container">
		<div class="row">
			<div class=" col-md-10 col-md-offset-1 text-center">
				<h2>Por que estudar na Toth?</h2>
				<p class="lead">
					Lorem ipsum dolor sit amet, ius minim gubergren ad. <br>
				At mei sumo sonet audiam, ad mutat elitr platonem vix. Ne nisl idque fierent vix. Ferri clitaponderum ne.
				</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<div class="feature">
					<i class="icon-trophy"></i>
					<h3>Professores experientes</h3>
					<p>
						Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
					</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="feature">
					<i class=" icon-ok-4"></i>
					<h3>Certificação confiável</h3>
					<p>
						Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
					</p>	
				</div>
			</div>
			<div class="col-md-6">
				<div class="feature">
					<i class="icon-mic"></i>
					<h3>+500 Aulas em áudio</h3>
					<p>
					Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
					</p>
				</div>
			</div>
			<div class="col-md-6">
				<div class="feature">
					<i class="icon-video"></i>
					<h3>+1.200 Vídeo aulas</h3>
					<p>
						Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex, appareat similique an usu.
					</p>
				</div>
			</div>
		</div>
	</div>
</section>



<section id="main_content">
	<div class="container">
		<div class="row add_bottom_30">
			<div class="col-md-12 text-center">
				<h2>Próximos Cursos</h2>
				<p class="lead">Veja abaixo os cursos que estão por vir.</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-6">
				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Controlador Lógico Programável (CLP) – Nível I</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>Preparar o aluno para interagir com controladores lógicos programáveis e entender melhor o universo de programação industrial.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 30 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Introdução</li>
											<li>Sistemas numéricos.</li>
											<li>Teoria de CLP (Hardware).</li>
											<li>Linguagens de programação conforme IEC 61131-3.</li>
											<li>Fundamentos de lógica de programação.</li>
										</ul>										
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>

				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Redes industriais e sistemas supervisórios – Nível I</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>Fornecer ao aluno conhecimentos sobre redes, os protocolos mais comuns ao ambiente industrial e conceitos sobre sistemas de supervisão e monitoramento de processos.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 30 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Teoria e arquitetura de redes.</li>
											<li>Principais protocolos de comunicação industrial.</li>
											<li>Supervisórios SCADA e suas aplicações.</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Eletrônica digital – Nível I</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>Fornecer ao aluno uma base de conhecimentos sólida na área de eletrônica digital e lógica.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 30 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Sistemas Numéricos.</li>
											<li>Álgebra booleana e as portas lógicas.</li>
											<li>Circuitos Combinatórios e Sequenciais.</li>
											<li>Circuitos Microcontrolados e Microprocessados.</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Marketing</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>O curso tem como objetivo levar o aluno (a) á compreender e analisar os conceitos de marketing no mercado de negócios e ampliar sua visão como um todo.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 60 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Definição de marketing.</li>
											<li>Ambiente de marketing.</li>
											<li>SIM – Sistema de informação em marketing.</li>
											<li>Analise de mercado do consumidor.</li>
											<li>Produtos, serviços, marcas e embalagens.</li>
											<li>Plano de marketing.</li>
											<li>Implementação, controle e serviços em marketing.</li>
											<li>Marketing Sensorial</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6">
				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Comandos elétricos – Nível I</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>Fornecer ao aluno conhecimentos nos componentes mais utilizados em comandos elétricos industriais, além de trazer comandos elétricos bastante usuais em plantas fabris.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 30 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Introdução a comandos elétricos.</li>
											<li>Dispositivos de potência.</li>
											<li>Dispositivos de proteção e comando.</li>
											<li>Simbologia.</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Microcontroladores (família 8051) – Nível I</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>Fornecer ao aluno sólidos conhecimentos a respeito dos microcontroladores, especialmente os da família 8051, e noções de programação em linguagem C e Assembly.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 30 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Histórico dos microcontroladores.</li>
											<li>Família 8051 – Informações gerais.</li>
											<li>Hardware – estrutura e principais funções.</li>
											<li>Linguagens de programação.</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Desenvolvimento de plano de negócio</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>O curso tem como objetivo desenvolver competência á elaborar um plano de negócios.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 40 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Empreendedorismo.</li>
											<li>Plano de negócio.</li>
											<li>Análise dos mercados e análise SWOT .</li>
											<li>Estratégias de negócio.</li>
											<li>Plano de marketing.</li>
											<li>Plano de recursos humanos.</li>
											<li>Plano operacional.</li>
											<li>Plano financeiro</li>
											<li>Viabilidade do negócio.</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>
				<div class="media">
					<div class="circ-wrapper pull-left">
						<h3>01<br>Set</h3>
					</div>
					<div class="media-body">
						<h4 class="media-heading">
							<a href="javascript:void(0)">Direito empresarial</a> 
							<span class="label label-warning">Novo</span>
						</h4>
						<p>O curso tem como objetivo proporcionar ao aluno conhecimento do direito empresarial, sua origem e finalidade.</p>
						<ul class="data-lessons">
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-clock"></i>Duração: 40 horas
								</a>
								<div class="po-content hidden">
									<div class="po-title">
										<strong>Conteúdo</strong>
									</div>
									<div class="po-body">
										<ul class="no_margin">
											<li>Noções preliminares de direito.</li>
											<li>Pessoa Jurídica.</li>
											<li>Direito empresarial.</li>
											<li>Estabelecimento comercial.</li>
											<li>Tipos de sociedades empresariais.</li>
											<li>Microempresa e empresa de pequeno porte.</li>
											<li>Títulos de crédito.</li>
											<li>Falência.</li>
											<li>Recuperação judicial e extrajudicial.</li>
											<li>Contratos.</li>
										</ul>
									</div>
								</div>
							</li>
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-slideshare"></i>Slides
								</a>
							</li>							
							<li class="po-markup">
								<a class="po-link" href="javascript:void(0)" >
									<i class="icon-doc-text-inv"></i>Apostila
								</a>
							</li>
						</ul>
					</div>
				</div>					
			</div>
		</div>
	</div>
</section>