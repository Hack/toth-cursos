<section id="login_bg">
<div  class="container">
<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div id="login">
			<h4 class="text-center">
				Cadastrar
			</h4>
			<hr>
			<form action="<?php echo base_url('cadastro/salvar');?>" method="POST">
				<div class="form-group">
					<input type="text" class=" form-control required"  placeholder="Nome" name="nome">
					<span class="input-icon"><i class=" icon-user"></i></span>
				</div>
				<div class="form-group">
					<input type="text" class=" form-control required" placeholder="Email" name="email">
					<span class="input-icon"><i class="icon-email"></i></span>
				</div>
				<div class="form-group">
					<input type="text" class=" form-control required" id="password1" placeholder="Senha" name="senha">
					<span class="input-icon"><i class=" icon-lock"></i></span>
				</div>
				<div class="form-group">
					<input type="text" class=" form-control required" id="password2" placeholder="Confirmar Senha" name="confirmar_senha">
					<span class="input-icon"><i class=" icon-lock"></i></span>
				</div>
                <div id="pass-info" class="clearfix"></div>
				<button class="button_fullwidth">Criar Conta</button>
			</form>
		</div>
	</div>
</div>
</div>
</section><!-- End register -->