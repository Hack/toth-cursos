<section id="sub-header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<h1>Toth Cursos Online</h1>
				<p class="lead boxed">Nova maneira de aprender</p>
			</div>
		</div>
	</div>
	<div class="divider_top"></div>
</section>

<section id="main_content">
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="<?php echo base_url();?>">Home</a></li>
			<li class="active">Institucional</li>
		</ol>

		<div class="row">
			<div class="col-md-7">
				<h3>Quem Somos?</h3>
				<p>
					A empresa Toth Cursos Online foi fundada no ano de 2014 na cidade de Guarulhos- SP. Sua principal missão é ser uma organização de excelência e referência em educação online , oferecendo uma  educação inovadora, que promova o desenvolvimento e uma inovação tecnológica.
				</p>
				<p>
					A Toth Cursos online conta com docentes qualificados, especialistas em suas áreas.
				</p>

				<h3>Princípios</h3>

				<h4>Missão</h4>
				<p>
					Ser uma organização de excelência e referência em educação online , oferecendo uma  educação inovadora, que promova o desenvolvimento e uma inovação tecnológica. 
				</p>

				<h4>Visão</h4>
				<p>
					Contribuir para o desenvolvimento humano e da comunidade por meio da educação online e da inovação tecnológica, visando na formação de cidadãos críticos, conscientes e empreendedores.
				</p>

				<h4>Valores</h4>
				<ul>
					<li>Ter o foco nos estudantes e nos professores;</li>
					<li>Respeito às pessoas, ao meio ambiente e à cultura;</li>
					<li>Compromisso com a excelência;</li>
					<li>Atitude empreendedora.</li>
				</ul>
			</div>
			<div class="col-md-4 col-md-offset-1">
				<div class="row">
					<div class="col-md-12 col-sm-6">
						<div class="thumbnail">
							<div class="project-item-image-container cursor-default">
								<img src="<?php echo base_url('public/img/institucional/institucional-1.jpg');?>" alt="Ensino a distância" width="360" height="221"/>
							</div>
						</div>
					</div>

					<div class="col-md-12  col-sm-6">
						<div class="thumbnail">
							<div class="project-item-image-container cursor-default">
								<img src="<?php echo base_url('public/img/institucional/institucional-2.jpg');?>" alt="Educação inovadora" width="360" height="221"/>d
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<hr>

		<div class="row">
			<?php
			if(!empty($professores)){
				foreach($professores as $professor){
			?>
					<div class="col-md-4 col-sm-6">
						<div class=" box_style_3">
							<p>
								<img src='<?php echo base_url("public/img/{$professor['imagem']}");?>' alt="<?php echo $professor['professor'];?>" class="img-circle styled" width="200" height="200">
							</p>
							<h4 class="p-title"><?php echo $professor['professor'];?> </h4>
							<p>
								<?php echo $professor['apresentacao'];?>
							</p>   
							<ul class="social_team">
								<li><a href="#"><i class="icon-facebook"></i></a></li>
								<li><a href="#"><i class="icon-twitter"></i></a></li>
								<li><a href="#"><i class=" icon-google"></i></a></li>
							</ul>
							<hr>
							<a href='<?php echo base_url("cursos/professor/{$professor['url']}");?>' class=" button_medium_outline">Visualizar Cursos</a>
						</div>
					</div>
			<?php
				}
			}
			?>
		</div>
	</div>
</section>