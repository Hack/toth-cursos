<section id="sub-header">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-center">
				<h1>Contato</h1>
				<p class="lead boxed">Deixe sua dúvida, sugestão ou reclamação</p>
			</div>
		</div>
	</div>
	<div class="divider_top"></div>
</section>

  
<section id="main_content" >
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<h3>Endereço</h3>
				<ul id="contact-info">
					<li><i class="icon-home"></i> Guarulhos, São Paulo</li>
					<li><i class="icon-phone"></i> (11) xxxx-xxxx</li>
					<li><i class=" icon-email"></i> <a href="#">contato@tothcursos.com.br</a></li>
				</ul>
				<hr>
				<h3>Siga-nos</h3>
				<p>
					Siga-nos em nossas redes sociais e fique por dentro de todas as novidades oferecidas pela Toth Cursos.
				</p>
				<ul id="follow_us_contacts">
					<li><a href="#"><i class="icon-facebook"></i>fb.com/username</a></li>
					<li><a href="#"><i class="icon-twitter"></i>twitter.com/#username</a></li>
					<li><a href="#"><i class=" icon-google"></i>googleplus.com/username</a></li>
				</ul>
			</div>
			<div class="col-md-8">
				<div class=" box_style_2">
					<span class="tape"></span>
					<div class="row">
						<div class="col-md-12">
							<h3>Contato</h3>
						</div>
					</div>
					<div id="message-contact"></div>
					<form method="post" action="<?php echo base_url('contato/enviar');?>" id="contactform">
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control style_2" id="name_contact" name="nome" placeholder="Nome" required>
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" class="form-control style_2" id="lastname_contact" name="sobrenome" placeholder="Sobrenome" required>
									<span class="input-icon"><i class="icon-user"></i></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="email" id="email_contact" name="email" class="form-control style_2" placeholder="Email" required>
									<span class="input-icon"><i class="icon-email"></i></span>
								</div>
							</div>
							<div class="col-md-6 col-sm-6">
								<div class="form-group">
									<input type="text" id="phone_contact" name="telefone" class="form-control style_2" placeholder="Telefone" required>
									<span class="input-icon"><i class="icon-phone"></i></span>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<textarea rows="5" id="message_contact" name="mensagem" class="form-control" placeholder="Escreva sua mensagem" style="height:200px;"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<input type="submit" value="Enviar" class=" button_medium" id="submit-contact"/>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>