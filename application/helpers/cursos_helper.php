<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('formata_professor'))
{
    function formata_professor($professores = array())
    {
		if(!empty($professores)){
			foreach ($professores as $key => $value){
				$professores[$key]['url'] = strtolower(convert_accented_characters(str_replace(" ", "-", $value['professor'])));
			}

			return $professores;
		}else{
			return false;
		}
    }   
  
}

if ( ! function_exists('formata_curso'))
{
    function formata_curso($cursos = array())
    {
		if(!empty($cursos)){
			foreach ($cursos as $key => $value){
				$cursos[$key]['url'] = strtolower(convert_accented_characters(str_replace(" ", "-", $value['curso'])));
				$cursos[$key]['url_categoria'] = strtolower(convert_accented_characters(str_replace(" ", "-", $value['categoria'])));
				$cursos[$key]['resumo'] = character_limiter($value['objetivo'],108);
				$cursos[$key]['preco'] = str_replace(".", ",", $value['preco']);
			}

			return $cursos;
		}else{
			return false;
		}
    }    
}

if ( ! function_exists('formata_quantidade'))
{
	function formata_quantidade($quantidade = array())
	{
		if(!empty($quantidade)){
			foreach ($quantidade as $key => $value){
				$quantidade[$key]['url'] = strtolower(convert_accented_characters(str_replace(" ", "-", $value['categoria'])));
			}
		}

		return $quantidade;
	} 
}