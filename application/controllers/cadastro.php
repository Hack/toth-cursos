<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends CI_Controller {

	private $title = "Cadastro - Toth Cursos Online";
	private $header;

	public function _remap($method){
		if(method_exists("Cadastro", $method)){
			$this->$method();
		}else{
			 $this->index();
		}
	}

	function __construct()
	{
		parent::__construct();
		$this->load->model("Cadastro_model");

		$this->header['title'] = $this->title;
	}

	public function index()
	{
		$this->load->view('layout/header',$this->header);
		$this->load->view('cadastro/index');
		$this->load->view('layout/footer');
	}	

	public function salvar()
	{
		// echo "ASDASD";exit;
		var_dump($this->Cadastro_model->Salvar($this->input->post()));exit;
		redirect();
	}
}