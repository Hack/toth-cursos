<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sistema extends CI_Controller {

	public function _remap($method){
		if(method_exists("Sistema", $method)){
			$this->$method();
		}else{
			 show_404();
		}
	}	

	private function minha_conta()
	{
		$this->load->view('layout/header');
		$this->load->view('sistema/minha-conta');
		$this->load->view('layout/footer');
	}

}