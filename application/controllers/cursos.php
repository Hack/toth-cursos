<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cursos extends CI_Controller {

	private $title = "Cursos - Toth Cursos Online";
	private $header;	

	public function _remap($method){
		if(method_exists("Cursos", $method)){
			$this->$method();
		}else{
			 $this->index();
		}
	}

	function __construct()
	{
		parent::__construct();
		$this->load->model("Curso_model");

		$this->header['title'] = $this->title;
	}

	public function index()
	{
		$data['quantidade_categorias'] = formata_quantidade($this->Curso_model->QuantidadeCursosCategoria());
		$data['professores'] = formata_professor($this->Curso_model->Professores());
		$data['cursos'] = formata_curso($this->Curso_model->Get());

		$this->load->view('layout/header',$this->header);
		$this->load->view('cursos/index',$data);
		$this->load->view('layout/footer');
	}

	public function visualizar()
	{
		$this->load->view('layout/header',$this->header);
		$this->load->view('cursos/visualizar');
		$this->load->view('layout/footer');		
	}

	public function professor()
	{
		$professor = $this->uri->segment(3);
		$data['professor_ativo'] = $professor;

		if(!empty($professor)){
			$professor = strtolower(str_replace("-", " ", $professor));

			$busca = array(
				"P.professor" => array("tipo" => "string", "valor" => $professor)
			);

			$data['quantidade_categorias'] = formata_quantidade($this->Curso_model->QuantidadeCursosCategoria());
			$data['professores'] = formata_professor($this->Curso_model->Professores());
			$data['cursos'] = formata_curso($this->Curso_model->Get($busca));

			$this->load->view('layout/header',$this->header);
			$this->load->view('cursos/index',$data);
			$this->load->view('layout/footer');		

		}else{
			redirect("cursos/");
		}
	}

	public function categoria()
	{
		$categoria = $this->uri->segment(3);
		$data['categoria_ativa'] = $categoria;

		if(!empty($categoria)){
			$categoria = strtolower(str_replace("-", " ", $categoria));

			$busca = array(
				"CC.categoria" => array("tipo" => "string", "valor" => $categoria)
			);

			$data['quantidade_categorias'] = formata_quantidade($this->Curso_model->QuantidadeCursosCategoria());
			$data['professores'] = formata_professor($this->Curso_model->Professores());
			$data['cursos'] = formata_curso($this->Curso_model->Get($busca));

			$this->load->view('layout/header',$this->header);
			$this->load->view('cursos/index',$data);
			$this->load->view('layout/footer');

		}else{
			redirect("cursos/");
		}		
	}
}