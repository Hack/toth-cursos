<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contato extends CI_Controller {

	private $title = "Contato - Toth Cursos Online";
	private $header;

	public function _remap($method){
		if(method_exists("Contato", $method)){
			$this->$method();
		}else{
			 $this->index();
		}
	}

	function __construct()
	{
		parent::__construct();
		$this->load->model("Contato_model");

		$this->header['title'] = $this->title;
	}

	public function index()
	{
		$this->load->view('layout/header',$this->header);
		$this->load->view('contato/index');
		$this->load->view('layout/footer');
	}

	public function enviar()
	{
		$dados = $this->input->post();
		echo $this->Contato_model->Enviar($dados);
	}
}