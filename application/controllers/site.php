<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends CI_Controller {

	public function _remap($method){
		if(PAGINA_ENTRADA){
			if(!$this->input->cookie("PAGINA_ENTRADA")){
				$this->input->set_cookie("PAGINA_ENTRADA",true,3600);
				$this->pagina_entrada();
			}else if(method_exists("Site", $method)){
				$this->$method();
			}else{
				 $this->index();
			}
		}else if(method_exists("Site", $method)){
			$this->$method();
		}else{
			 $this->index();
		}
	}

	function __construct()
	{
		parent::__construct();
	}	

	public function pagina_entrada()
	{
		$this->load->view('site/pagina-entrada');
	}

	public function index()
	{
		$this->load->view('layout/header');
		$this->load->view('site/index');
		$this->load->view('layout/footer');
	}

	public function cadastro()
	{
		$this->load->view('layout/header');
		$this->load->view('site/cadastro');
		$this->load->view('layout/footer');
	}	

	public function newsletter()
	{
		if($this->input->server('REQUEST_METHOD') == "POST"){
			if(!empty($this->input->post("email_newsletter"))){
				$this->load->model('Newsletter_model');
				echo $this->Newsletter_model->Inscrever($this->input->post("email_newsletter"));
			}
		}else{
			redirect('/', 'refresh');
		}
	}
}