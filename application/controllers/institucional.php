<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Institucional extends CI_Controller {

	private $title = "Institucional - Toth Cursos Online";
	private $header;

	public function _remap($method){
		if(method_exists("Institucional", $method)){
			$this->$method();
		}else{
			 $this->index();
		}
	}

	function __construct()
	{
		parent::__construct();
		$this->load->model("Professor_model");

		$this->header['title'] = $this->title;
	}

	public function index()
	{
		$data['professores'] = $this->Professor_model->Get();

		if(!empty($data['professores'])){
			foreach ($data['professores'] as $key => $value){
				$data['professores'][$key]['url'] = strtolower(convert_accented_characters(str_replace(" ", "-", $value['professor'])));
			}
		}

		$this->load->view('layout/header',$this->header);
		$this->load->view('institucional/index',$data);
		$this->load->view('layout/footer');		
	}
}