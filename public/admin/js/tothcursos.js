var Geral = {

	init: function(){
		self = this;		
	}
};

var Banner = {

	init: function(){
		self = this;		
	},

	Tabela: function(){
		$('#tabela-banner').dataTable({
		   	"sPaginationType": "full_numbers",
		   	"aaSortingFixed": [[0,'asc']],
		    "oLanguage": {
				"sUrl": base_url + "public/admin/language/data-table-pt-br.lang",
		    }
		});		
	}
}