var Login = {
	init: function(){
		self = this;
	},

	Logar: function(){
		$(".mensagem-erro").html("");

		if($('#usuario').val() == '' || $('#senha').val() == '') {
			if($('#usuario').val() == ''){
				$('#usuario').addClass('error');
			}else{
				$('#usuario').removeClass('error');
			}

			if($('#senha').val() == ''){
				$('#senha').addClass('error'); 
			}else{
				$('#senha').removeClass('error');
			}

			$(".mensagem-erro").html("Usuário/senha inválido.");

			return false;
		}else{
			return true;
		}
	}
}

$(document).ready(function(){
	Login.init();
});