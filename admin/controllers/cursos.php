<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Cursos extends CI_Controller {

	function __construct()
	{
    	parent::__construct();
  	}

    public function _remap($method)
    {
        if($this->session->userdata('logged_in')){        
            $this->$method();
        }else{
            redirect('login', 'refresh');
        }      
    }    

    public function index()
    {
        $data['breadcrumb'] = array(
        	array(
	            "titulo" => "Cursos",
	            "controller" => $this->router->fetch_class(),
	            "action" => ""
            )          
        );

        $data['titulo'] = "Cursos";

    	$this->load->view('layout/header',$data);
    	$this->load->view('cursos/index');
    	$this->load->view('layout/footer');
    }

}

?>