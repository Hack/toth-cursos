<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
session_start();

class Dashboard extends CI_Controller {

    function __construct()
    {
        parent::__construct();
    }

    public function _remap($method)
    {
        if($this->session->userdata('logged_in')){        
            $this->$method();
        }else{
            redirect('login', 'refresh');
        }      
    }

    public function index()
    {
        $data['titulo'] = "Dashboard";

        $this->load->view('layout/header',$data);
        $this->load->view('dashboard/index');
        $this->load->view('layout/footer');
    }
  
}

?>