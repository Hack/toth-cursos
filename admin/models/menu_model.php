<?php
Class Menu_model extends CI_Model
{
	public function CarregarMenu()
	{
		if($this->session->userdata('logged_in') == true){

			$sql = $this->db->query("SELECT * FROM admin_permissao_perfil WHERE id_perfil = ? ORDER BY ordem ASC",array($this->session->userdata('id_perfil')));

			if($sql->num_rows() > 0){
				return $sql->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	public function EspacoUsadoBancoDados()
	{
		if($this->session->userdata('logged_in') == true){
			$sql = $this->db->query("
				SELECT s.SCHEMA_NAME AS `Database`, s.DEFAULT_CHARACTER_SET_NAME AS `Charset`,
					COUNT(t.TABLE_NAME) AS `Tables`,

					(SELECT COUNT(*) FROM information_schema.ROUTINES AS r
						WHERE r.routine_schema = s.SCHEMA_NAME) AS `Routines`,

					 round(SUM(t.DATA_LENGTH + t.INDEX_LENGTH) / 1048576 ,1) AS `size`

					FROM information_schema.SCHEMATA AS s
						LEFT JOIN information_schema.TABLES t ON s.schema_name = t.table_schema
					WHERE s.SCHEMA_NAME NOT IN ('information_schema', 'performance_schema')

				GROUP BY s.SCHEMA_NAME;
			");

			if($sql->num_rows() > 0){
				return $sql->result_array();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
}
?>