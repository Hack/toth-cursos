<div class="leftpanel">
	
    <div class="logopanel">
    	<h1><a href="<?php echo base_url('dashboard');?>">Toth Cursos</a></h1>
    </div>
    
    <div class="datewidget">Olá, <?php echo $this->session->userdata('nome');?>!</div>

	<div class="searchwidget">
    	<form action="results.html" method="post">
        	<div class="input-append">
                <input type="text" class="span2 search-query" placeholder="Busca">
                <button type="submit" class="btn"><span class="icon-search"></span></button>
            </div>
        </form>
    </div><!--searchwidget-->
    
    <div class="plainwidget">
    	<h3>Servidor</h3>
    	<?php
	    	$banco_dados = $this->Menu_model->EspacoUsadoBancoDados();

	    	if(!empty($banco_dados)){
	    		$divisao = $banco_dados[1]['size']/1000;

	    		if($divisao > 1){
					$usado = round($divisao)."GB";
					$porcentagem_total = ($usado * 100) / 5000;
					$porcentagem_progress = round($porcentagem_total);
	    		}else{
	    			$usado = $banco_dados[1]['size']." MB";
	    			$porcentagem_total = ($usado * 100) / 5000;
	    			$porcentagem_progress = round($porcentagem_total);
	    		}
	    	}else{
	    		$usado = "0 MB";
	    		$porcentagem = 0;
	    		$porcentagem_progress = 0;
	    	}
    	?>
    
    	<small>Usado <?php echo $usado;?> de 5 GB </small>
    	<div class="progress progress-info">
            <div class="bar" style="width: <?php echo $porcentagem_progress;?>%"></div>
        </div>
        <small><strong><?php echo $porcentagem_total;?>% total</strong></small>

    	<h3>Banco de dados</h3>
    	<?php
	    	$banco_dados = $this->Menu_model->EspacoUsadoBancoDados();

	    	if(!empty($banco_dados)){
	    		$divisao = $banco_dados[1]['size']/1000;

	    		if($divisao > 1){
					$usado = round($divisao)."GB";
					$porcentagem_total = ($usado * 100) / 5000;
					$porcentagem_progress = round($porcentagem_total);
	    		}else{
	    			$usado = $banco_dados[1]['size']." MB";
	    			$porcentagem_total = ($usado * 100) / 5000;
	    			$porcentagem_progress = round($porcentagem_total);
	    		}
	    	}else{
	    		$usado = "0 MB";
	    		$porcentagem = 0;
	    		$porcentagem_progress = 0;
	    	}
    	?>
    
    	<small>Usado <?php echo $usado;?> de 5 GB </small>
    	<div class="progress progress-info">
            <div class="bar" style="width: <?php echo $porcentagem_progress;?>%"></div>
        </div>
        <small><strong><?php echo $porcentagem_total;?>% total</strong></small>
    </div>

    <div class="leftmenu">        
        <ul class="nav nav-tabs nav-stacked">
			<?php
				if($this->uri->segment(1) == 'dashboard'){
					$class = "class='active'";
				}else{
					$class="";
				}
			?>            
            <li <?php echo $class;?>>
            	<a href="<?php echo base_url('dashboard');?>">
            		<span class="icon-home"></span> Dashboard
        		</a>
    		</li>

			<?php
				$menu = $this->Menu_model->CarregarMenu();

				if(!empty($menu)){
					foreach($menu as $value){
						if($value['controller'] == $this->uri->segment(1)){
							$class = "class='active'";
						}else{
							$class="";
						}						
			?>
			<li <?php echo $class;?>>
				<a href="<?php echo base_url("{$value['controller']}/");?>">
					<span class="<?php echo $value['icone'];?>"></span> <?php echo $value['descricao'];?>
				</a>
			</li>
			<?php
					}
				}
			?>        		
        </ul>
    </div><!--leftmenu-->
    
</div>